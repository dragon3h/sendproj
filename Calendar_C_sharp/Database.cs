﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace ProjectUp
{
    class Database
    {
        public Database()
        {
            Connection = new SqlConnection(@"Data Source=p6rbnv719l.database.windows.net;Initial Catalog=calendar2;Integrated Security=False;User ID=ipd7abbott;Password=JohnAbbott2000;Connect Timeout=60;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            Connection.Open();
        }

        private SqlConnection Connection;
//---------------------Roma start--------------------------------
        public void addEvent(EventCal ev)
        {
            using(SqlCommand cmd = new SqlCommand())
            {
                DateTime st = ev.StartTime;
                DateTime et = ev.EndTime;
                string title = ev.Title;
                string description = ev.Description;
                double latitude = ev.Latitude;
                double longitude = ev.Longitude;
                string invited = ev.Invited;

                try
                {
                    cmd.CommandText = "INSERT INTO CalendarEvent (StartTime, Endtime, Title, Description, Latitude, Longitude, Invited) VALUES (@StartTime, @Endtime, @Title, @Description, @Latitude,@Longitude,@Invited)";
                    cmd.Parameters.AddWithValue("@StartTime", st);
                    cmd.Parameters.AddWithValue("@Endtime", et);
                    cmd.Parameters.AddWithValue("@Title", title);
                    cmd.Parameters.AddWithValue("@Description", description);
                    cmd.Parameters.AddWithValue("@Latitude", latitude);
                    cmd.Parameters.AddWithValue("@Longitude", longitude);
                    cmd.Parameters.AddWithValue("@Invited", invited);


                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = Connection;
                    cmd.ExecuteNonQuery();
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (InvalidOperationException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (SqlException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        public void deleteEventByID(int id)
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                try
                {
                    cmd.CommandText = "DELETE FROM CalendarEvent WHERE ID=@id";
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = Connection;
                    cmd.ExecuteNonQuery();
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (InvalidOperationException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (SqlException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            }
        }

        public List<EventCal> getEventByDayTime(DateTime dtStWeek, DateTime dtEndWeek)
        {
            List<EventCal> resultList = new List<EventCal>();
            using (SqlCommand cmd = new SqlCommand())
            {
                try
                {
                    cmd.CommandText = "SELECT * FROM CalendarEvent WHERE StartTime  BETWEEN @dtStWeek AND @dtEndWeek";
                    cmd.Parameters.AddWithValue("@dtStWeek", dtStWeek);
                    cmd.Parameters.AddWithValue("@dtEndWeek", dtEndWeek);

                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = Connection;
                    cmd.ExecuteNonQuery();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int ID = reader.GetInt32(0);
                            string title = reader.GetString(1);
                            string description = reader.GetString(2) as string;
                            DateTime startTime = reader.GetDateTime(3);
                            DateTime endTime = reader.GetDateTime(4);
                            double latitude = reader.GetDouble(5);
                            double longitude = reader.GetDouble(6);
                            string invited = reader.GetString(7);
                            EventCal eve = new EventCal(startTime, endTime, title, description, latitude, longitude);
                            eve.Invited = invited as string;
                            resultList.Add(eve);
                        }
                    }
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (InvalidOperationException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (SqlException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            }

            return resultList;
        }

        public EventCal showPickedEvent(DateTime dt)
        {
            EventCal ev = null;
                using (SqlCommand cmd = new SqlCommand())
                {
                try
                {
                    DateTime date = dt;

                    cmd.CommandText = "SELECT * FROM CalendarEvent WHERE @date >= StartTime AND @date < EndTime";
                    cmd.Parameters.AddWithValue("@date", date);

                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = Connection;
                    cmd.ExecuteNonQuery();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int ID = reader.GetInt32(0);
                            string title = reader.GetString(1);
                            string description = reader.GetString(2);
                            DateTime startTime = reader.GetDateTime(3);
                            DateTime endTime = reader.GetDateTime(4);
                            double latitude = reader.GetDouble(5);
                            double longitude = reader.GetDouble(6);
                            string invited = reader.GetString(7) as string;

                            ev = new EventCal(startTime, endTime, title, description, latitude, longitude);
                            ev.ID = ID;
                            ev.Invited = invited;
                        }
                    }
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (InvalidOperationException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (InvalidCastException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (SqlException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            }
            return ev;
        }

        //----------------------Roma end----------------------------
        //----------------------Ievgen start-------------------------

        public void addPerson(Person p)
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "INSERT  INTO CalendarContacts (FirstName,SecondName,Photo,BirthDate,Sex,Street,Appartement,City,PostalCode,EmailAddress,Phone) VALUES (@FirstName, @SecondName,@Photo,@BirthDate,@Sex,@Street,@Appartement,@City,@PostalCode,@EmailAddress,@Phone)";
                cmd.Parameters.AddWithValue("@FirstName", p.FirstName);
                cmd.Parameters.AddWithValue("@SecondName", p.SecondName);
                cmd.Parameters.AddWithValue("@Photo", p.Photo);
                if (p.BirthDate != null)   
                {
                    cmd.Parameters.AddWithValue("@BirthDate", p.BirthDate);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@BirthDate", DBNull.Value);
                }

                
                cmd.Parameters.AddWithValue("@Sex", p.Sex);
                cmd.Parameters.AddWithValue("@Street", p.Street);
                if (p.Appartement != null)
                {
                    cmd.Parameters.AddWithValue("@Appartement", p.Appartement);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@Appartement", DBNull.Value);
                }

               
                cmd.Parameters.AddWithValue("@City", p.City);
                cmd.Parameters.AddWithValue("@PostalCode", p.PostalCode);
                cmd.Parameters.AddWithValue("@EmailAddress", p.EmailAddress);
                cmd.Parameters.AddWithValue("@Phone", p.Phone);

                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = Connection;
                cmd.ExecuteNonQuery();
            }
        }

        public void deletePerson(int id)
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "DELETE FROM CalendarContacts WHERE id = @id";
                cmd.Parameters.AddWithValue("@id", id);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = Connection;
                cmd.ExecuteNonQuery();
            }
        }
        public void updatePerson(int id,Person p)
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "UPDATE CalendarContacts SET FirstName=@firstName, SecondName=@SecondName,Photo=@Photo,BirthDate=@Birthdate,Sex=@Sex,Street=@Street,Appartement=@Appartement,City=@City,PostalCode=@PostalCode,EmailAddress=@EmailAddress,Phone=@Phone WHERE id=@id";
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@FirstName", p.FirstName);
                cmd.Parameters.AddWithValue("@SecondName", p.SecondName);
                cmd.Parameters.AddWithValue("@Photo", p.Photo);
                if (p.BirthDate != null)
                {
                    cmd.Parameters.AddWithValue("@BirthDate", p.BirthDate);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@BirthDate", DBNull.Value);
                }


                cmd.Parameters.AddWithValue("@Sex", p.Sex);
                cmd.Parameters.AddWithValue("@Street", p.Street);
                if (p.Appartement != null)
                {
                    cmd.Parameters.AddWithValue("@Appartement", p.Appartement);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@Appartement", DBNull.Value);
                }


                cmd.Parameters.AddWithValue("@City", p.City);
                cmd.Parameters.AddWithValue("@PostalCode", p.PostalCode);
                cmd.Parameters.AddWithValue("@EmailAddress", p.EmailAddress);
                cmd.Parameters.AddWithValue("@Phone", p.Phone);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = Connection;
                cmd.ExecuteNonQuery();
            }
        }

        public List<Person> getAllPerson()
        {
            List<Person> personList = new List<Person>();
            using (SqlCommand cmd = new SqlCommand("SELECT * FROM CalendarContacts", Connection))
            {
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int ID = reader.GetInt32(0);
                        string firstName = reader.GetString(1);
                        string secondName = reader.GetString(2);
                        byte[] photo = (byte[])reader[3];

                        int x = reader.GetOrdinal("BirthDate");
                        DateTime? birthDate = reader.IsDBNull(x) ? (DateTime?)null : reader.GetDateTime(x);

                        string sex = reader.GetString(5) == null ? "" : reader.GetString(5);
                        string street = reader.GetString(6) as string;

                        int y = reader.GetOrdinal("Appartement");
                        int? appartement = reader.IsDBNull(y) ? (int?)null : reader.GetInt32(y);

                        
                        string city = reader.GetString(8) as string;
                        string postalCode = reader.GetString(9) as string;
                        string emailAddress = reader.GetString(10) as string;
                        string phone = reader.GetString(11) as string;
                         
                        Person p = new Person(firstName, secondName, photo, birthDate, sex, street, appartement, city, postalCode, emailAddress, phone);
                        p.ID = ID;
                        if (birthDate != null)
                        {
                            DateTime bday = birthDate.Value;
                            DateTime today = DateTime.Today;
                           
                            int age = today.Year - bday.Year;

                            if (bday > today.AddYears(-age))
                                age--;

                            p.Age = age.ToString();
                        }
                        else p.Age = "N/A";
                        personList.Add(p);
                    }
                }
            }
            return personList;
        }

        //----------------------Ievgen finich------------------------
    }
}
