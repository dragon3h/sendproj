﻿using Microsoft.Maps.MapControl.WPF;
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.Text.RegularExpressions;
using System.Threading;
using Microsoft.Maps.MapControl.WPF.Design;


using ProjectUp.GeocodeService;
using ProjectUp.ImageryService;
using ProjectUp.RouteService;
using ProjectUp.SearchService;
using System.Windows.Media.Media3D;

namespace ProjectUp
{
//------------------------Ievgen Start----------------------------------------------------
    class Person
    {
        private static int id = 0;
        public Person( string firstName,string secondName, byte [] photo, DateTime ? birthDate, string sex, string street, int ? appartement, string city, string postalCode, string emailAddress, string phone )
        {
            ID = System.Threading.Interlocked.Increment(ref id); FirstName = firstName; SecondName = secondName; Photo = photo; BirthDate = birthDate; Sex = sex; Street = street;
            Appartement = appartement; City = city; PostalCode = postalCode; EmailAddress = emailAddress; Phone = phone;
        }
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public byte []  Photo { get; set; }
        public DateTime ? BirthDate { get; set; }
        public string Sex { get; set; }
        public string Street { get; set; }
        public int ? Appartement { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string EmailAddress { get; set; }
        public string Phone { get; set; }
        public string Age { get; set; }
    }

    
 //-----------------------------Ievgen Finish---------------------------------------------------  
    //public enum WeekDay { Mon, Tue, Wen, Thu, Fri, Sat, Sun}
    class Event
    {
        public Event(string title, string start, string end, string lacation, string description, string guest)
        {
            Title = title;
            Start = start;
            End = end;
            Location = Location;
            Description = description;
            Guest = guest;
        }
        public string Title { set; get; }
        public string Start { set; get; }
        public string End { set; get; }
        public string Location { set; get; }
        public string Description { set; get; }
        public string Guest { set; get; }
    }
   
    public partial class MainWindow : Window
    {
        // ---------------Roman start ----------------------------------------------------------------------------

        static string[] WeekDay = new string[] { "Mon", "Tue", "Wen", "Thu", "Fri", "Sat", "Sun" };
        static List<EventCal> EventsList = new List<EventCal>();
        static List<DateTime> CurrentWeekView = new List<DateTime>();
        static List<Label> listOfMonthDays = new List<Label>();
        static int[,] daysOfMonth = new int[6, 7];
        int currentMonthNum = 0;
        EventCal pickedEvent;
        Label monthName;
        double stepWidth = 50;
        double stepHeight = 25;
        int heightMonthName = 25;
        int heightCanvas = 625;
        int hourWidth = 45;
        double stepHeightMonth = 25;
        int row = 0;
        int col = 0;
        static DateTime dt = DateTime.Now;
        static DateTime actualDate = dt;
        static int wd = (int)dt.DayOfWeek;
        Database db;
        private bool weekButtonClicked = true;
        private bool monthButtonClicked = false;
        private bool dayButtonClicked = false;
        Microsoft.Maps.MapControl.WPF.Pushpin pushpin;

        //------------------Roman end -----------------------------------------------------------------------------------

        public MainWindow()
        {
            //------Ievgen start--------
            InitializeComponent();

            db = new Database();
            lvPersonList.ItemsSource = db.getAllPerson();
            lvPersonList.Items.Refresh();

            if (lvPersonList.Items.Count == 0) buttonAddNew.Visibility = Visibility.Visible;
            else buttonAddNew.Visibility = Visibility.Hidden;

            fillComboboxesTime();
            myMap.Focus();
            UpdateBackPatternWeek(null, null);
            //------Ievgen end---------

        }

        //-------------Roman start-----------------------------------------------------------------------------------      

        public void fillComboboxesTime()
        {
            try
            {
                for (int i = 0; i < 24; i++)
                {
                    string hour = i + ":00";
                    comboBoxStTime.Items.Add(hour);
                    tbEndTime.Items.Add(hour);
                }
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e.Message);
            }
            
        }

        void UpdateBackPatternWeek(object sender, SizeChangedEventArgs e)
        {
            if (weekButtonClicked)
            {
                drawCanvasGridWeek();

                weekDays(actualDate, hourWidth, 0);
                dayHoures();

                // show events in current week
                EventsList = db.getEventByDayTime(firstWeekDay(actualDate), lastWeekDay(actualDate));
                showWeekEvent(actualDate);
            }

            if (dayButtonClicked)
            {
                drawCanvasGridDay();

                weekDays(actualDate, hourWidth, 0);
                dayHoures();

                // show events in current week

                EventsList = db.getEventByDayTime(startDay(actualDate), endDay(actualDate));

                showDayEvent(actualDate);
                // create Z-Index
            }

            if (monthButtonClicked)
            {
                drawCanvasGridMonth();

                headerForMonth(actualDate, 0, heightMonthName);
                monthDays(actualDate);
                // show events in current week

                var firstDayOfMonth = new DateTime(actualDate.Year, actualDate.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                EventsList = db.getEventByDayTime(firstDayOfMonth, lastDayOfMonth);
                showMonthEvent(actualDate);
            }

            // create Z-Index
            //Canvas.SetZIndex(canvCalView, 1);
        }

        void AddLineToBackground(double x1, double y1, double x2, double y2)
        {
            var line = new Line()
            {
                X1 = x1,
                Y1 = y1,
                X2 = x2,
                Y2 = y2,
                Stroke = System.Windows.Media.Brushes.Black,
                StrokeThickness = 1,
                SnapsToDevicePixels = true
            };
            try
            {
                line.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
            
            canvCalView.Children.Add(line);
        }

        private void OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (monthButtonClicked)
                {
                    // Retrieve the coordinate of the mouse position.
                    System.Windows.Point pt = e.GetPosition((UIElement)sender);

                    // Perform the hit test against a given portion of the visual object tree.
                    HitTestResult result = VisualTreeHelper.HitTest(canvCalView, pt);

                    if (result != null)
                    {
                        // Perform action on hit visual object.
                        row = (int)(pt.Y - (heightMonthName + stepHeight)) / (int)stepHeightMonth;
                        col = (int)pt.X / (int)stepWidth;

                        string dateStr = monthName.Content.ToString();
                        DateTime pickEventCol = Convert.ToDateTime(dateStr);

                        TimeSpan ts = new TimeSpan(0, 0, 0);
                        if (daysOfMonth[row, col] != 0)
                        {
                            actualDate = pickEventCol.AddDays(daysOfMonth[row, col] - 1) + ts;
                            drawCanvasGridDay();
                            clearInputEvent();
                            dayDate(actualDate);
                            CurrentWeekView.Clear();
                            CurrentWeekView.Add(actualDate);
                            setEventDescrForDay();
                            dayHoures();

                            // show events in current day
                            EventsList = db.getEventByDayTime(startDay(actualDate), endDay(actualDate));

                            showDayEvent(actualDate);

                            // create Z-Index
                            Canvas.SetZIndex(canvCalView, 1);

                            dayButtonClicked = true;
                            weekButtonClicked = false;
                            monthButtonClicked = false;
                        }
                    }
                }

                if (weekButtonClicked)
                {
                    string pickDate = "";
                    string pickDateTime = "";
                    DateTime pickEvent;
                    weekDays(actualDate, hourWidth, 0);

                    // Retrieve the coordinate of the mouse position.
                    System.Windows.Point pt = e.GetPosition((UIElement)sender);

                    // Perform the hit test against a given portion of the visual object tree.
                    HitTestResult result = VisualTreeHelper.HitTest(canvCalView, pt);

                    if (result != null)
                    {
                        // Perform action on hit visual object.
                        row = (int)(pt.Y / stepHeight) - 1;
                        col = (int)(pt.X - hourWidth) / (int)stepWidth + 1;

                        switch (col)
                        {
                            case 1:
                                {
                                    DateTime pickEventCol = CurrentWeekView[0];
                                    TimeSpan ts = new TimeSpan(0, 0, 0);
                                    pickEventCol = pickEventCol.Date + ts;
                                    pickDate = pickEventCol.ToShortDateString();
                                }
                                break;
                            case 2:
                                {
                                    DateTime pickEventCol = CurrentWeekView[1];
                                    TimeSpan ts = new TimeSpan(0, 0, 0);
                                    pickEventCol = pickEventCol.Date + ts;
                                    pickDate = pickEventCol.ToShortDateString();
                                }
                                break;
                            case 3:
                                {
                                    DateTime pickEventCol = CurrentWeekView[2];
                                    TimeSpan ts = new TimeSpan(0, 0, 0);
                                    pickEventCol = pickEventCol.Date + ts;
                                    pickDate = pickEventCol.ToShortDateString();
                                }
                                break;
                            case 4:
                                {
                                    DateTime pickEventCol = CurrentWeekView[3];
                                    TimeSpan ts = new TimeSpan(0, 0, 0);
                                    pickEventCol = pickEventCol.Date + ts;
                                    pickDate = pickEventCol.ToShortDateString();
                                }
                                break;
                            case 5:
                                {
                                    DateTime pickEventCol = CurrentWeekView[4];
                                    TimeSpan ts = new TimeSpan(0, 0, 0);
                                    pickEventCol = pickEventCol.Date + ts;
                                    pickDate = pickEventCol.ToShortDateString();
                                }
                                break;
                            case 6:
                                {
                                    DateTime pickEventCol = CurrentWeekView[5];
                                    TimeSpan ts = new TimeSpan(0, 0, 0);
                                    pickEventCol = pickEventCol.Date + ts;
                                    pickDate = pickEventCol.ToShortDateString();
                                }
                                break;
                            case 7:
                                {
                                    DateTime pickEventCol = CurrentWeekView[6];
                                    TimeSpan ts = new TimeSpan(0, 0, 0);
                                    pickEventCol = pickEventCol.Date + ts;
                                    pickDate = pickEventCol.ToShortDateString();
                                }
                                break;
                        }

                        pickDateTime = pickDate + " " + row + ":00";
                        pickEvent = Convert.ToDateTime(pickDateTime);
                        pickedEvent = db.showPickedEvent(pickEvent);

                        if (pickedEvent == null)
                        {
                            clearInputEvent();
                            tbStartDate.Text = pickDate;
                            comboBoxStTime.Text = row + ":00";
                            tbTitle.Focus();
                        }
                        else
                        {
                            setEventDescription();
                            myMap.Children.Remove(pin);
                            myMap.Children.Remove(pushpin);
                            pushpin = new Microsoft.Maps.MapControl.WPF.Pushpin();
                            pushpin.Location = new Microsoft.Maps.MapControl.WPF.Location(pickedEvent.Latitude, pickedEvent.Longitude);
                            myMap.Children.Add(pushpin);
                            myMap.SetView(new Microsoft.Maps.MapControl.WPF.Location(pickedEvent.Latitude, pickedEvent.Longitude), 16);
                        }
                    }
                }

                if (dayButtonClicked)
                {
                    string pickDateDay = "";
                    string pickDateTimeDay = "";
                    DateTime pickEventDay;
                    // Retrieve the coordinate of the mouse position.
                    System.Windows.Point pt = e.GetPosition((UIElement)sender);

                    // Perform the hit test against a given portion of the visual object tree.
                    HitTestResult result = VisualTreeHelper.HitTest(canvCalView, pt);

                    if (result != null)
                    {
                        row = (int)(pt.Y / stepHeight) - 1;
                        col = (int)(pt.X - hourWidth) / (int)stepWidth + 1;

                        DateTime pickEventCol = actualDate;
                        TimeSpan ts = new TimeSpan(0, 0, 0);
                        pickEventCol = pickEventCol.Date + ts;
                        pickDateDay = pickEventCol.ToShortDateString();
                        pickDateTimeDay = pickDateDay + " " + row + ":00";
                        pickEventDay = Convert.ToDateTime(pickDateTimeDay);
                        pickedEvent = db.showPickedEvent(pickEventDay);

                        if (pickedEvent == null)
                        {
                            clearInputEvent();
                            tbStartDate.Text = pickDateDay;
                            comboBoxStTime.Text = row + ":00";
                            tbTitle.Focus();
                        }
                        else
                        {
                            setEventDescription();
                            myMap.Children.Remove(pin);
                            myMap.Children.Remove(pushpin);
                            pushpin = new Microsoft.Maps.MapControl.WPF.Pushpin();
                            pushpin.Location = new Microsoft.Maps.MapControl.WPF.Location(pickedEvent.Latitude, pickedEvent.Longitude);
                            myMap.Children.Add(pushpin);
                            myMap.SetView(new Microsoft.Maps.MapControl.WPF.Location(pickedEvent.Latitude, pickedEvent.Longitude), 16);
                        }
                    }
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            
        }

        public void createHour(Label lbl, string hour, int num)
        {
            lbl.HorizontalContentAlignment = HorizontalAlignment.Center;
            lbl.VerticalContentAlignment = VerticalAlignment.Center;
            lbl.FontSize = 14;
            lbl.Padding = new Thickness(0, 0, 0, 2);
            lbl.Width = hourWidth;
            lbl.Height = stepHeight;
            lbl.BorderBrush = System.Windows.Media.Brushes.Black;
            lbl.BorderThickness = new Thickness(1);
            Canvas.SetTop(lbl, stepHeight + stepHeight * num);
            Canvas.SetLeft(lbl, 0);
            lbl.Content = hour;
            lbl.Background = System.Windows.Media.Brushes.Azure;
            canvCalView.Children.Add(lbl);
        }

        public void dayHoures()
        {
            Label lbl00 = new Label();
            createHour(lbl00, "0:00", 0);
            Label lbl01 = new Label();
            createHour(lbl01, "1:00", 1);
            Label lbl02 = new Label();
            createHour(lbl02, "2:00", 2);
            Label lbl03 = new Label();
            createHour(lbl03, "3:00", 3);
            Label lbl04 = new Label();
            createHour(lbl04, "4:00", 4);
            Label lbl05 = new Label();
            createHour(lbl05, "5:00", 5);
            Label lbl06 = new Label();
            createHour(lbl06, "6:00", 6);
            Label lbl07 = new Label();
            createHour(lbl07, "7:00", 7);
            Label lbl08 = new Label();
            createHour(lbl08, "8:00", 8);
            Label lbl09 = new Label();
            createHour(lbl09, "9:00", 9);
            Label lbl10 = new Label();
            createHour(lbl10, "10:00", 10);
            Label lbl11 = new Label();
            createHour(lbl11, "11:00", 11);
            Label lbl12 = new Label();
            createHour(lbl12, "12:00", 12);
            Label lbl13 = new Label();
            createHour(lbl13, "13:00", 13);
            Label lbl14 = new Label();
            createHour(lbl14, "14:00", 14);
            Label lbl15 = new Label();
            createHour(lbl15, "15:00", 15);
            Label lbl16 = new Label();
            createHour(lbl16, "16:00", 16);
            Label lbl17 = new Label();
            createHour(lbl17, "17:00", 17);
            Label lbl18 = new Label();
            createHour(lbl18, "18:00", 18);
            Label lbl19 = new Label();
            createHour(lbl19, "19:00", 19);
            Label lbl20 = new Label();
            createHour(lbl20, "20:00", 20);
            Label lbl21 = new Label();
            createHour(lbl21, "21:00", 21);
            Label lbl22 = new Label();
            createHour(lbl22, "22:00", 22);
            Label lbl23 = new Label();
            createHour(lbl23, "23:00", 23);
        }

        public void createWeekDay(Label lbl, int num, int step, int stepTop)
        {
            lbl.HorizontalContentAlignment = HorizontalAlignment.Center;
            lbl.VerticalContentAlignment = VerticalAlignment.Center;
            lbl.FontSize = 14;
            lbl.Padding = new Thickness(0, 0, 2, 0);
            lbl.Width = stepWidth;
            lbl.Height = stepHeight;
            lbl.BorderBrush = System.Windows.Media.Brushes.Black;
            lbl.BorderThickness = new Thickness(1);
            Canvas.SetTop(lbl, stepTop);
            Canvas.SetLeft(lbl, num * stepWidth + step);
            lbl.Background = System.Windows.Media.Brushes.Azure;
            canvCalView.Children.Add(lbl);
        }

        public void createMonthDay(Label lbl, int numX, int numY)
        {
            lbl.HorizontalContentAlignment = HorizontalAlignment.Center;
            lbl.VerticalContentAlignment = VerticalAlignment.Center;
            lbl.FontSize = 12;
            lbl.Padding = new Thickness(0, 0, 2, 0);
            lbl.Width = 25;
            lbl.Height = 25;
            double stepHeightDayOfMonth = heightMonthName + stepHeight;
            Canvas.SetTop(lbl, stepHeightDayOfMonth + numY * stepHeightMonth);
            Canvas.SetLeft(lbl, numX * stepWidth);
            canvCalView.Children.Add(lbl);
        }

        public void dayDate(DateTime dt)
        {
            try
            {
                Label lblDay = new Label();
                createWeekDay(lblDay, 0, hourWidth, 0);
                lblDay.Content = dt.ToString("ddd M/d");
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void weekDays(DateTime dt, int step, int stepTop)
        {
            List<Label> listOfLabels = new List<Label>();

            Label lblMn = new Label();
            createWeekDay(lblMn, 0, step, stepTop);
            listOfLabels.Insert(0, lblMn);
            Label lblTu = new Label();
            createWeekDay(lblTu, 1, step, stepTop);
            listOfLabels.Insert(1, lblTu);
            Label lblWn = new Label();
            createWeekDay(lblWn, 2, step, stepTop);
            listOfLabels.Insert(2, lblWn);
            Label lblTh = new Label();
            createWeekDay(lblTh, 3, step, stepTop);
            listOfLabels.Insert(3, lblTh);
            Label lblFr = new Label();
            createWeekDay(lblFr, 4, step, stepTop);
            listOfLabels.Insert(4, lblFr);
            Label lblSa = new Label();
            createWeekDay(lblSa, 5, step, stepTop);
            listOfLabels.Insert(5, lblSa);
            Label lblSu = new Label();
            createWeekDay(lblSu, 6, step, stepTop);
            listOfLabels.Insert(6, lblSu);


            int dayNow = (int)dt.DayOfWeek - 1;
            //MessageBox.Show("" + dayNow);

            CurrentWeekView.Clear();

            DateTime dtTemp;

            try
            {
                for (int i = 0; i < 7; i++)
                {
                    if (dayNow > -1)
                    {
                        if (dayNow != i)
                        {
                            dtTemp = dt.AddDays(i - dayNow);
                            listOfLabels[i].Content = dtTemp.ToString("ddd d/M");
                            CurrentWeekView.Add(dtTemp);
                        }
                        else
                        {
                            listOfLabels[i].Content = dt.ToString("ddd d/M");
                            CurrentWeekView.Add(dt);
                        }
                    }
                    if (dayNow == -1)
                    {
                        dtTemp = dt.AddDays(dayNow - 5 + i);
                        listOfLabels[i].Content = dtTemp.ToString("ddd M/d");
                        CurrentWeekView.Add(dtTemp);
                    }
                }

                foreach (DateTime cdt in CurrentWeekView)
                {
                    Label actWeekDay = new Label();
                    if (cdt.Day == DateTime.Now.Day)
                    {
                        actWeekDay.Width = stepWidth;
                        actWeekDay.Opacity = .4;
                        actWeekDay.Height = heightCanvas - stepHeight;
                        actWeekDay.BorderBrush = System.Windows.Media.Brushes.Black;
                        actWeekDay.BorderThickness = new Thickness(1);
                        Canvas.SetTop(actWeekDay, stepHeight);
                        int nm = (int)cdt.DayOfWeek;
                        if (nm == 0)
                        {
                            nm = 7;
                        }
                        Canvas.SetLeft(actWeekDay, (nm - 1) * stepWidth + hourWidth);
                        actWeekDay.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#77B4F2"));
                        canvCalView.Children.Add(actWeekDay);
                    }
                }
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void monthDays(DateTime dt)
        {
            try
            {
                var firstDayOfMonth = new DateTime(dt.Year, dt.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                var lastDayOfMonthBefor = firstDayOfMonth.AddDays(-1);
                int startWeekDay = (int)firstDayOfMonth.DayOfWeek;

                if (startWeekDay == 0)
                {
                    startWeekDay = 7;
                }

                int count = 1;
                int countMonthBefor = lastDayOfMonthBefor.Day - startWeekDay + 2;
                int countMonthAfter = 1;
                string nowDate = DateTime.Now.ToShortDateString();
                int nowDateNum = Int32.Parse(nowDate.Substring(0, 2));

                for (int i = 0; i < 6; i++)
                {
                    for (int j = 0; j < 7; j++)
                    {
                        if (i == 0 && j < startWeekDay - 1)
                        {
                            Label lbl = new Label();
                            createMonthDay(lbl, j, i);
                            listOfMonthDays.Insert(i, lbl);
                            lbl.Content = countMonthBefor;
                            countMonthBefor++;
                        }
                        else if (count <= lastDayOfMonth.Day)
                        {
                            Label lbl = new Label();
                            daysOfMonth[i, j] = count;
                            createMonthDay(lbl, j, i);
                            listOfMonthDays.Insert(i, lbl);
                            lbl.Content = count;
                            lbl.FontWeight = FontWeights.Bold;
                            lbl.FontSize = 14;
                            count++;
                            //create Z-Index
                            Canvas.SetZIndex(lbl, 11);

                            if (daysOfMonth[i, j] == nowDateNum && currentMonthNum == DateTime.Now.Month)
                            {
                                Label lblActual = new Label();
                                lblActual.Width = stepWidth;
                                lblActual.Height = stepHeightMonth;
                                lblActual.BorderBrush = System.Windows.Media.Brushes.Black;
                                lblActual.BorderThickness = new Thickness(1, 1, 1, 1);
                                double stepHeightDayOfMonth = heightMonthName + stepHeight;
                                Canvas.SetTop(lblActual, stepHeightDayOfMonth + i * stepHeightMonth);
                                Canvas.SetLeft(lblActual, j * stepWidth);
                                lblActual.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#77B4F2"));
                                canvCalView.Children.Add(lblActual);

                            }
                        }
                        else if (i > 3 && count > lastDayOfMonth.Day)
                        {
                            Label lbl = new Label();
                            createMonthDay(lbl, j, i);
                            listOfMonthDays.Insert(i, lbl);
                            lbl.Content = countMonthAfter;
                            countMonthAfter++;
                        }
                    }
                }
                actualDate = firstDayOfMonth;
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void headerForMonth(DateTime dt, int step, int stepTop)
        {
            List<Label> listOfLabels = new List<Label>();

            try
            {
                monthName = new Label();

                currentMonthNum = actualDate.Month;
                string monthShow = actualDate.ToString("MMMM") + " " + actualDate.Year.ToString();
                monthName.Content = monthShow;
                monthName.Width = canvCalView.ActualWidth;
                monthName.HorizontalContentAlignment = HorizontalAlignment.Center;
                monthName.VerticalContentAlignment = VerticalAlignment.Center;
                monthName.Padding = new Thickness(5, 0, 7, 0);

                monthName.FontSize = 16;
                monthName.FontWeight = FontWeights.Bold;
                Canvas.SetTop(monthName, 0);
                Canvas.SetLeft(monthName, 0);

                canvCalView.Children.Add(monthName);

                Label lblMn = new Label();
                createWeekDay(lblMn, 0, step, stepTop);
                listOfLabels.Insert(0, lblMn);
                Label lblTu = new Label();
                createWeekDay(lblTu, 1, step, stepTop);
                listOfLabels.Insert(1, lblTu);
                Label lblWn = new Label();
                createWeekDay(lblWn, 2, step, stepTop);
                listOfLabels.Insert(2, lblWn);
                Label lblTh = new Label();
                createWeekDay(lblTh, 3, step, stepTop);
                listOfLabels.Insert(3, lblTh);
                Label lblFr = new Label();
                createWeekDay(lblFr, 4, step, stepTop);
                listOfLabels.Insert(4, lblFr);
                Label lblSa = new Label();
                createWeekDay(lblSa, 5, step, stepTop);
                listOfLabels.Insert(5, lblSa);
                Label lblSu = new Label();
                createWeekDay(lblSu, 6, step, stepTop);
                listOfLabels.Insert(6, lblSu);

                int dayNow = (int)dt.DayOfWeek - 1;

                CurrentWeekView.Clear();

                for (int i = 0; i < 7; i++)
                {
                    if (dayNow > -1)
                    {
                        if (dayNow != i)
                        {
                            DateTime dtTemp = dt.AddDays(i - dayNow);
                            listOfLabels[i].Content = dtTemp.ToString("ddd");
                            CurrentWeekView.Add(dtTemp);
                        }
                        else
                        {
                            listOfLabels[i].Content = dt.ToString("ddd");
                            CurrentWeekView.Add(dt);
                        }
                    }
                    if (dayNow == -1)
                    {
                        DateTime dtSun = dt.AddDays(dayNow - 5 + i);
                        listOfLabels[i].Content = dtSun.ToString("ddd");
                        CurrentWeekView.Add(dtSun);
                    }
                }
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void btBefor_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (weekButtonClicked)
                {
                    canvCalView.Children.Clear();
                    drawCanvasGridWeek();
                    dayHoures();
                    actualDate = actualDate.AddDays(-7);
                    CurrentWeekView.Clear();
                    weekDays(actualDate, hourWidth, 0);
                    EventsList = db.getEventByDayTime(firstWeekDay(actualDate), lastWeekDay(actualDate));
                    showWeekEvent(actualDate);
                }

                if (dayButtonClicked)
                {
                    canvCalView.Children.Clear();
                    drawCanvasGridDay();
                    dayHoures();
                    actualDate = actualDate.AddDays(-1);
                    CurrentWeekView.Clear();
                    dayDate(actualDate);
                    setEventDescrForDay();
                    EventsList = db.getEventByDayTime(startDay(actualDate), endDay(actualDate));

                    showDayEvent(actualDate);
                }

                if (monthButtonClicked)
                {
                    canvCalView.Children.Clear();
                    drawCanvasGridMonth();
                    var firstDayOfMonth = new DateTime(actualDate.Year, actualDate.Month, 1);
                    actualDate = firstDayOfMonth.AddMonths(-1);
                    CurrentWeekView.Clear();
                    headerForMonth(actualDate, 0, heightMonthName);
                    monthDays(actualDate);

                    firstDayOfMonth = new DateTime(actualDate.Year, actualDate.Month, 1);
                    var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                    EventsList = db.getEventByDayTime(firstDayOfMonth, lastDayOfMonth);

                    showMonthEvent(actualDate);
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void btForward_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (weekButtonClicked)
                {
                    canvCalView.Children.Clear();
                    drawCanvasGridWeek();
                    dayHoures();
                    actualDate = actualDate.AddDays(7);
                    CurrentWeekView.Clear();
                    weekDays(actualDate, hourWidth, 0);
                    EventsList = db.getEventByDayTime(firstWeekDay(actualDate), lastWeekDay(actualDate));
                    showWeekEvent(actualDate);
                }

                if (dayButtonClicked)
                {
                    CurrentWeekView.Clear();
                    canvCalView.Children.Clear();
                    drawCanvasGridDay();
                    dayHoures();
                    actualDate = actualDate.AddDays(1);
                    dayDate(actualDate);
                    setEventDescrForDay();
                    EventsList = db.getEventByDayTime(startDay(actualDate), endDay(actualDate));

                    showDayEvent(actualDate);
                }

                if (monthButtonClicked)
                {
                    canvCalView.Children.Clear();
                    drawCanvasGridMonth();
                    var firstDayOfMonth = new DateTime(actualDate.Year, actualDate.Month, 1);
                    actualDate = firstDayOfMonth.AddMonths(1);
                    CurrentWeekView.Clear();
                    headerForMonth(actualDate, 0, heightMonthName);
                    monthDays(actualDate);

                    firstDayOfMonth = new DateTime(actualDate.Year, actualDate.Month, 1);
                    var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                    EventsList = db.getEventByDayTime(firstDayOfMonth, lastDayOfMonth);

                    showMonthEvent(actualDate);
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            string dateStart = tbStartDate.Text + " " + comboBoxStTime.Text;
            DateTime st = Convert.ToDateTime(dateStart);
            string dateEnd = tbEndDate.Text + " " + tbEndTime.Text;
            DateTime et = Convert.ToDateTime(dateEnd);
            string title = tbTitle.Text;
            string description = tbDescription.Text;
            double latitude = 0;
            double longitude = 0;
            string invited = lableInvited.Content.ToString();

            if (pinLocation != null)
            {
                latitude = pinLocation.Latitude;
                longitude = pinLocation.Longitude;
            }
                

            EventCal ev = new EventCal(st, et, title, description,latitude,longitude);
            ev.Invited = invited;
            db.addEvent(ev);

            clearInputEvent();
            CurrentWeekView.Clear();

            if (dayButtonClicked)
            {
                drawCanvasGridDay();
                dayDate(actualDate);
                CurrentWeekView.Add(actualDate);

                dayHoures();

                // show events in current day
                EventsList = db.getEventByDayTime(startDay(actualDate), endDay(actualDate));

                showDayEvent(actualDate);
            }

            if (weekButtonClicked)
            {
                drawCanvasGridWeek();

                weekDays(actualDate, hourWidth, 0);
                dayHoures();
                EventsList = db.getEventByDayTime(firstWeekDay(actualDate), lastWeekDay(actualDate));
                showWeekEvent(actualDate);
            }
        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            int id = pickedEvent.ID;
            db.deleteEventByID(id);
            clearInputEvent();
            CurrentWeekView.Clear();

            if (dayButtonClicked)
            {
                drawCanvasGridDay();               
                dayDate(actualDate);
                CurrentWeekView.Add(actualDate);

                dayHoures();

                // show events in current day
                EventsList = db.getEventByDayTime(startDay(actualDate), endDay(actualDate));

                showDayEvent(actualDate);
            }

            if (weekButtonClicked)
            {
                drawCanvasGridWeek();

                weekDays(actualDate, hourWidth, 0);
                dayHoures();
                EventsList = db.getEventByDayTime(firstWeekDay(actualDate), lastWeekDay(actualDate));
                showWeekEvent(actualDate);
            }

            if (monthButtonClicked)
            {
            }
            
        }

        private void showCurrentEventOfWeek(EventCal ev, int wdEvent, int currentEv)
        {
            Label lbl = new Label();
            lbl.Width = stepWidth;
            lbl.Padding = new Thickness(0, 0, 2, 0);
            string stTime = ev.StartTime.ToString("HH:mm");
            int hourStart = Int32.Parse(stTime.Substring(0, 2));
            string endTime = ev.EndTime.ToString("HH:mm");
            int hourEnd = Int32.Parse(endTime.Substring(0, 2));
            lbl.Height = (hourEnd - hourStart) * stepHeight;
            Canvas.SetTop(lbl, hourStart * stepHeight + stepHeight);
            Canvas.SetLeft(lbl, hourWidth + stepWidth * wdEvent);
            lbl.Background = System.Windows.Media.Brushes.Blue;
            lbl.Content = EventsList[currentEv].Title;
            canvCalView.Children.Add(lbl);
        }

        private void showCurrentEventOfMonth(EventCal ev, int currentEv, int nm)
        {
            Label lbl = new Label();
            lbl.Width = stepWidth;
            lbl.Height = 30;

            int stDate = ev.StartTime.Day;
            int endDate = ev.EndTime.Day;

            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    if (daysOfMonth[i, j] == stDate)
                    {
                        Canvas.SetTop(lbl, (heightMonthName + stepHeight) + stepHeightMonth * i + lbl.Height * nm + 25);
                        Canvas.SetLeft(lbl, stepWidth * j);
                    }
                }
            }

            lbl.Background = System.Windows.Media.Brushes.Blue;
            lbl.BorderBrush = System.Windows.Media.Brushes.Black;
            lbl.BorderThickness = new Thickness(1);
            lbl.Content = EventsList[currentEv].Title;
            canvCalView.Children.Add(lbl);
        }

        private void showCurrenteventOfDay(EventCal ev, int currentEv)
        {
            Label lbl = new Label();
            lbl.Width = stepWidth;
            lbl.Padding = new Thickness(0, 0, 2, 0);
            string stTime = ev.StartTime.ToString("HH:mm");
            int hourStart = Int32.Parse(stTime.Substring(0, 2));
            string endTime = ev.EndTime.ToString("HH:mm");
            int hourEnd = Int32.Parse(endTime.Substring(0, 2));
            lbl.Height = (hourEnd - hourStart) * stepHeight;
            Canvas.SetTop(lbl, hourStart * stepHeight + stepHeight);
            Canvas.SetLeft(lbl, hourWidth);
            lbl.Background = System.Windows.Media.Brushes.Blue;
            lbl.Content = EventsList[currentEv].Title;
            canvCalView.Children.Add(lbl);
        }

        private void showWeekEvent(DateTime dt)
        {
            int currentEv = 0;
            foreach (EventCal ev in EventsList)
            {

                if (ev.StartTime >= firstWeekDay(dt) && ev.StartTime <= lastWeekDay(dt))
                {
                    int wdActual = (int)ev.StartTime.DayOfWeek;
                    if (wdActual == 0)
                    {
                        wdActual = 7;
                    }
                    showCurrentEventOfWeek(ev, wdActual - 1, currentEv);
                    currentEv++;
                }
            }
        }

        private void showMonthEvent(DateTime dt)
        {
            List<EventCal> currentMonthEvents = new List<EventCal>();

            try
            {
                var firstDayOfMonth = new DateTime(dt.Year, dt.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                int currentEv = 0;
                foreach (EventCal ev in EventsList)
                {
                    if (ev.StartTime >= firstDayOfMonth && ev.StartTime <= lastDayOfMonth)
                    {
                        if (currentMonthEvents.Exists(x => x.StartTime.ToShortDateString() == ev.StartTime.ToShortDateString() && x.StartTime.ToShortTimeString() != ev.StartTime.ToShortTimeString()))
                        {
                            showCurrentEventOfMonth(ev, currentEv, 1);
                        }
                        else
                        {
                            showCurrentEventOfMonth(ev, currentEv, 0);
                            currentMonthEvents.Add(ev);
                        }
                        currentEv++;
                    }
                }
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
            }
            
        }

        private void showDayEvent(DateTime dt)
        {
            int currentEv = 0;
            foreach (EventCal ev in EventsList)
            {

                if (ev.StartTime >= startDay(dt) && ev.StartTime <= endDay(dt))
                {
                    showCurrenteventOfDay(ev, currentEv);
                    currentEv++;
                }
            }
        }

        private void drawCanvasGridMonth()
        {
            var w = canvCalView.ActualWidth;
            if (w == 0)
            {
                return;
            }

            stepWidth = w / 7;
            stepHeightMonth = (heightCanvas - heightMonthName - stepHeight) / 6;

            canvCalView.Children.Clear();

            // write grid (lines) in canva

            for (double x = stepWidth; x < w; x += stepWidth)
            {
                AddLineToBackground(x, heightMonthName + stepHeight, x, heightCanvas);
            }

            for (double y = heightMonthName + stepHeight + stepHeightMonth; y < heightCanvas; y += stepHeightMonth)
            {
                AddLineToBackground(0, y, w, y);
            }
        }

        private void drawCanvasGridWeek()
        {
            canvCalView.Children.Clear();
            var w = canvCalView.ActualWidth;
            if (w == 0)
            {
                return;
            }

            stepWidth = (w - hourWidth) / 7;

            // write grid (lines) in canvas
            AddLineToBackground(hourWidth, 0, hourWidth, heightCanvas);

            for (double x = hourWidth + stepWidth; x < w - w % 7 - hourWidth; x += stepWidth)
            {
                AddLineToBackground(x, 0, x, heightCanvas);
            }

            for (double y = stepHeight; y < heightCanvas; y += stepHeight)
            {
                AddLineToBackground(0, y, w, y);
            }
        }

        private void drawCanvasGridDay()
        {
            var w = canvCalView.ActualWidth;
            if (w == 0)
            {
                return;
            }

            stepWidth = (w - hourWidth);

            canvCalView.Children.Clear();

            // write grid (lines) in canvas
            AddLineToBackground(hourWidth, 0, hourWidth, heightCanvas);

                AddLineToBackground(hourWidth, 0, hourWidth, heightCanvas);
                AddLineToBackground(hourWidth + stepWidth, 0, hourWidth + stepWidth, heightCanvas);

            for (double y = stepHeight; y < heightCanvas; y += stepHeight)
            {
                AddLineToBackground(0, y, w, y);
            }
        }

        public DateTime startDay(DateTime dt)
        {
            DateTime stDay = new DateTime();
            try
            {
                TimeSpan ts = new TimeSpan(0, 0, 0);
                stDay = dt.Date + ts;
                
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
            }
            return stDay;
        }

        public DateTime endDay(DateTime dt)
        {
            DateTime endDay = new DateTime();
            try
            {
                TimeSpan ts = new TimeSpan(23, 59, 59);
                endDay = dt.Date + ts;
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
            }
            return endDay;
        }

        public DateTime firstWeekDay(DateTime dt)
        {
            DateTime monday = new DateTime();
            try
            {
                int delta = DayOfWeek.Monday - dt.DayOfWeek;
                monday = dt.AddDays(delta);
                TimeSpan ts = new TimeSpan(0, 0, 0);
                monday = monday.Date + ts;
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
            }
            return monday;
        }

        public DateTime lastWeekDay(DateTime dt)
        {
            DateTime sunday = new DateTime();
            try
            {
                int deltaLast = DayOfWeek.Sunday - dt.DayOfWeek + 7;
                sunday = dt.AddDays(deltaLast);
                TimeSpan tsSun = new TimeSpan(23, 59, 59);
                sunday = sunday.Date + tsSun;
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
            }
            return sunday;
        }

        public void clearInputEvent()
        {
            tbTitle.Text = "";
            comboBoxStTime.Text = "";
            tbStartDate.Text = "";
            tbEndTime.Text = "";
            tbEndDate.Text = "";
            tbDescription.Text = "";
            lableInvited.Content = "";
        }

        public void setEventDescription()
        {
            tbTitle.Text = pickedEvent.Title;
            tbStartDate.Text = pickedEvent.StartTime.ToShortDateString();
            comboBoxStTime.Text = pickedEvent.StartTime.ToShortTimeString();
            tbEndDate.Text = pickedEvent.EndTime.ToShortDateString();
            tbEndTime.Text = pickedEvent.EndTime.ToShortTimeString();
            tbDescription.Text = pickedEvent.Description;
            lableInvited.Content = pickedEvent.Invited;
        }

        public void setEventDescrForDay()
        {
            tbTitle.Text = "";
            tbEndTime.Text = "";
            tbDescription.Text = "";
            lableInvited.Content = "";

            tbStartDate.Text = actualDate.ToShortDateString();
            comboBoxStTime.Text = actualDate.ToShortTimeString();
            tbEndDate.Text = actualDate.ToShortDateString();
        }

        private void btDay_Click(object sender, RoutedEventArgs e)
        {
            drawCanvasGridDay();
            clearInputEvent();
            dayDate(actualDate);

            CurrentWeekView.Clear();
            CurrentWeekView.Add(actualDate);
            setEventDescrForDay();
            dayHoures();

            // show events in current day
            EventsList = db.getEventByDayTime(startDay(actualDate), endDay(actualDate));

            showDayEvent(actualDate);

            dayButtonClicked = true;
            weekButtonClicked = false;
            monthButtonClicked = false;
        }

        private void btWeek_Click(object sender, RoutedEventArgs e)
        {
            drawCanvasGridWeek();
            clearInputEvent();
            
            weekDays(actualDate, hourWidth, 0);
            dayHoures();
            CurrentWeekView.Clear();
            // show events in current week

            EventsList = db.getEventByDayTime(firstWeekDay(actualDate), lastWeekDay(actualDate));

            showWeekEvent(actualDate);

            dayButtonClicked = false;
            weekButtonClicked = true;
            monthButtonClicked = false;
        }

        private void btMonth_Click(object sender, RoutedEventArgs e)
        {
            drawCanvasGridMonth();
            clearInputEvent();

            headerForMonth(actualDate, 0, heightMonthName);
            monthDays(actualDate);
            CurrentWeekView.Clear();

            try
            {
                var firstDayOfMonth = new DateTime(actualDate.Year, actualDate.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                EventsList = db.getEventByDayTime(firstDayOfMonth, lastDayOfMonth);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
            
            showMonthEvent(actualDate);

            dayButtonClicked = false;
            weekButtonClicked = false;
            monthButtonClicked = true;
        }

        private void btDiscard_Click(object sender, RoutedEventArgs e)
        {
            clearInputEvent();
        }

        //------------Roman end------------------------------------------------------------------------------------

        //---------------------------------------Ievgen Start------------------------------------------------------
        //--------Initialize----------------


        int index = 0;
        OpenFileDialog op = new OpenFileDialog();
        LocationConverter locConverter = new LocationConverter();
        List<string> invitedList = new List<string>();

        private bool isValid()
        {
            String[] formats = { "yyyy M d", "yyyy/M/d", "yyyy-M-d", "yyyy:M:d", "d M yyyy", "d/M/yyyy", "d-M-yyyy", "d:M:yyyy", };
            DateTime expexteddate;
            if(textBoxBirthDate.Text != "") { 
                if (!(DateTime.TryParseExact(textBoxBirthDate.Text, formats, System.Globalization.CultureInfo.InvariantCulture,
                    DateTimeStyles.None, out expexteddate)))
            
                    return false;
            }
            if (textBoxFirstName.Text.Length < 3) {  return false; } 
            if (textBoxSecondName.Text.Length < 3) return false;
            if (textBoxPostal.Text.Length != 0 && textBoxPostal.Text.Length < 3) return false;
            
            return true;
        }
        private void SetTextChangedCommand(object sender, TextChangedEventArgs e)
        {
            
            TextBox box = sender as TextBox;
            if (box != null) { 
                battonAdd.IsEnabled = isValid();
                battonUpdate.IsEnabled = isValid();
            }

            if (textBoxFirstName.Text.Length < 3) { lableFirstChek.Content = "No"; lableFirstChek.Foreground = new SolidColorBrush(Colors.Red); } else { lableFirstChek.Content = "Ok"; lableFirstChek.Foreground = new SolidColorBrush(Colors.Green); }
            if (textBoxSecondName.Text.Length < 3) { lableSecondChek.Content = "No"; lableSecondChek.Foreground = new SolidColorBrush(Colors.Red); } else { lableSecondChek.Content = "Ok"; lableSecondChek.Foreground = new SolidColorBrush(Colors.Green); }
            DateTime expexteddate;
            String[] formats = { "yyyy M d", "yyyy/M/d", "yyyy-M-d", "yyyy:M:d", "d M yyyy", "d/M/yyyy", "d-M-yyyy", "d:M:yyyy", };
            if (!(DateTime.TryParseExact(textBoxBirthDate.Text, formats, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out expexteddate))) { lableBirthChek.Content = "No"; lableBirthChek.Foreground = new SolidColorBrush(Colors.Red); } else { lableBirthChek.Content = "Ok"; lableBirthChek.Foreground = new SolidColorBrush(Colors.Green); }

            if (textBoxPostal.Text.Length < 3) { lablePostalChek.Content = "No"; lablePostalChek.Foreground = new SolidColorBrush(Colors.Red); } else { lablePostalChek.Content = "Ok"; lablePostalChek.Foreground = new SolidColorBrush(Colors.Green); }
            if (textBoxStreet.Text.Length < 3) { lableStreetChek.Content = "No"; lableStreetChek.Foreground = new SolidColorBrush(Colors.Red); } else { lableStreetChek.Content = "Ok"; lableStreetChek.Foreground = new SolidColorBrush(Colors.Green); }
            if (textBoxCity.Text.Length < 3) { lableCityChek.Content = "No"; lableCityChek.Foreground = new SolidColorBrush(Colors.Red); } else { lableCityChek.Content = "Ok"; lableCityChek.Foreground = new SolidColorBrush(Colors.Green); }
            
            if (textBoxEmail.Text.Length < 3) { lableEmailChek.Content = "No"; lableEmailChek.Foreground = new SolidColorBrush(Colors.Red); } else { lableEmailChek.Content = "Ok"; lableEmailChek.Foreground = new SolidColorBrush(Colors.Green); }
            
            int parsedValue;
            if (!int.TryParse(textBoxApp.Text, out parsedValue)) { lableAppChek.Content = "No"; lableAppChek.Foreground = new SolidColorBrush(Colors.Red); } else { lableAppChek.Content = "Ok"; lableAppChek.Foreground = new SolidColorBrush(Colors.Green); }
            if (textBoxSecondName.Text.Length < 3) { lableSecondChek.Content = "No"; lableSecondChek.Foreground = new SolidColorBrush(Colors.Red); } else { lableSecondChek.Content = "Ok"; lableSecondChek.Foreground = new SolidColorBrush(Colors.Green); }
            if (textBoxPhone.Text.Length < 3) { lablePhoneChek.Content = "No"; lablePhoneChek.Foreground = new SolidColorBrush(Colors.Red); } else { lablePhoneChek.Content = "Ok"; lablePhoneChek.Foreground = new SolidColorBrush(Colors.Green); }
            
        }


        private Person setValueToPerson(){
        
        string firstName = textBoxFirstName.Text;
        string secondName = textBoxSecondName.Text;
        byte [] photo = getJPGFromImageControl(imgPhoto.Source as BitmapImage);

        DateTime? birthDate = string.IsNullOrEmpty(textBoxBirthDate.Text) ? (DateTime?)null : DateTime.Parse(textBoxBirthDate.Text);
        string sex = comboBoxSex.SelectedValue.ToString();
        string street = textBoxStreet.Text;
        int ? app = string.IsNullOrEmpty(textBoxApp.Text) ? (int?)null : int.Parse(textBoxApp.Text);
        string city = textBoxCity.Text;
        string postal =textBoxPostal.Text;
        string email = textBoxEmail.Text;
        string phone = textBoxPhone.Text;
        
        return new Person(firstName,secondName,photo,birthDate,sex,street,app,city,postal,email,phone);
    }   

    private void battonAdd_Click(object sender, RoutedEventArgs e)
        {

            db.addPerson(setValueToPerson());

            lvPersonList.ItemsSource = db.getAllPerson();
            lvPersonList.Items.Refresh();

            UpdateAddViewGrid.Visibility = Visibility.Hidden;
            ViewListGrid.Visibility = Visibility.Visible;
            battonAdd.Visibility = Visibility.Hidden;
            battonUpdate.Visibility = Visibility.Hidden;
        }

    private void battonDelete_Click(object sender, RoutedEventArgs e)
        {
            if (lvPersonList.SelectedIndex != -1)
            {
                index = lvPersonList.SelectedIndex;
                db.deletePerson(db.getAllPerson()[index].ID);
                lvPersonList.ItemsSource = db.getAllPerson();
                lvPersonList.Items.Refresh();
                textBoxFirstName.Clear(); textBoxSecondName.Clear();
            }
        }
       

    private void battonUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (lvPersonList.SelectedIndex != -1)
            {
                index = lvPersonList.SelectedIndex;

                db.updatePerson(db.getAllPerson()[index].ID,setValueToPerson());

                lvPersonList.ItemsSource = db.getAllPerson();
                lvPersonList.Items.Refresh();

                UpdateAddViewGrid.Visibility = Visibility.Hidden;
                ViewListGrid.Visibility = Visibility.Visible;
                battonAdd.Visibility = Visibility.Hidden;
                battonUpdate.Visibility = Visibility.Hidden;
            }
        }

        //From byte array to Windows.control.Image
    public BitmapImage ToImage(byte[] array)
        {
            using (var ms = new System.IO.MemoryStream(array))
            {
                var image = new BitmapImage();
                image.BeginInit();
                image.CacheOption = BitmapCacheOption.OnLoad; 
                image.StreamSource = ms;
                image.EndInit();
                return image;
            }
        }

    private void lvPersonList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvPersonList.SelectedIndex != -1)
            {
                index = lvPersonList.SelectedIndex;
                textBoxFirstName.Text = db.getAllPerson()[index].FirstName;
                textBoxSecondName.Text = db.getAllPerson()[index].SecondName;

                imgPhoto.Source = ToImage(db.getAllPerson()[index].Photo);
                imgPhotoLVG.Source = ToImage(db.getAllPerson()[index].Photo);

                textBoxBirthDate.Text = db.getAllPerson()[index].BirthDate!=null ?
                db.getAllPerson()[index].BirthDate.Value.ToString("yyyy/MM/dd") : "";

                comboBoxSex.SelectedValue = db.getAllPerson()[index].Sex;
                textBoxStreet.Text = db.getAllPerson()[index].Street;
                labelStreet.Content = db.getAllPerson()[index].Street;
                textBoxApp.Text = db.getAllPerson()[index].Appartement.ToString();
                labelApp.Content = db.getAllPerson()[index].Appartement.ToString();
                textBoxCity.Text = db.getAllPerson()[index].City;
                labelCity.Content = db.getAllPerson()[index].City;
                textBoxPostal.Text = db.getAllPerson()[index].PostalCode;
                labelPostal.Content = db.getAllPerson()[index].PostalCode;
                textBoxEmail.Text = db.getAllPerson()[index].EmailAddress;
                labelEmail.Content= db.getAllPerson()[index].EmailAddress;
                textBoxPhone.Text = db.getAllPerson()[index].Phone;
                labelPhone.Content= db.getAllPerson()[index].Phone;

               

            }
        }

    //From Windows.Control.Image to byte array
        public byte[] getJPGFromImageControl(BitmapImage imageC)
        {
            MemoryStream memStream = new MemoryStream();
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(imageC));
            encoder.Save(memStream);
            return memStream.ToArray();
        }
        
        private void ImageButton_Click(object sender, RoutedEventArgs e)
        {
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                if (!string.IsNullOrEmpty(op.FileName) && File.Exists(op.FileName))
                {
                    imgPhoto.Source = new BitmapImage(new Uri(op.FileName));
                }
                else
                {
                    MessageBox.Show("Soethig heppened with you file");
                }
            }
        }


        Microsoft.Maps.MapControl.WPF.Location pinLocation;
        Microsoft.Maps.MapControl.WPF.Pushpin pin;
        

    //static Location pinLocation;
    private void MapWithPushpins_MouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
                    
        myMap.Children.Remove(pin);
        myMap.Children.Remove(pushpin);

        // Disables the default mouse double-click action.
        e.Handled = true;
           

        //Get the mouse click coordinates
        var mousePosition = e.GetPosition(myMap);
        //Convert the mouse coordinates to a locatoin on the map
        pinLocation = myMap.ViewportPointToLocation(mousePosition);
            
        // The pushpin to add to the map.
        pin = new Microsoft.Maps.MapControl.WPF.Pushpin();
        pin.Location = pinLocation;

        // Adds the pushpin to the map.
        myMap.Children.Add(pin);

        if (myMap.ZoomLevel < 12) { myMap.SetView(pinLocation, 12); }
        else { myMap.SetView(pinLocation, myMap.ZoomLevel); }

    }

        //Show Map pushpin for selected event
        private void ChangeMapView_Click(object sender, RoutedEventArgs e)
        {
            // Parse the information of the button's Tag property
            string[] tagInfo = ((Button)sender).Tag.ToString().Split(' ');
            Microsoft.Maps.MapControl.WPF.Location center = (Microsoft.Maps.MapControl.WPF.Location)locConverter.ConvertFrom(tagInfo[0]);
            double zoom = System.Convert.ToDouble(tagInfo[1]);

            // Set the map view
            myMap.SetView(center, zoom);
        }

        //List view menu 
        private void OnListViewItem_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.Handled)
                return;

            ListViewItem item = MyVisualTreeHelper.FindParent<ListViewItem>((DependencyObject)e.OriginalSource);
            if (item == null)
                return;

            if (item.Focusable && !item.IsFocused)
                item.Focus();
        }
        public static class MyVisualTreeHelper
        {
            static bool AlwaysTrue<T>(T obj) { return true; }

            public static T FindParent<T>(DependencyObject child) where T : DependencyObject
            {
                return FindParent<T>(child, AlwaysTrue<T>);
            }

            public static T FindParent<T>(DependencyObject child, Predicate<T> predicate) where T : DependencyObject
            {
                DependencyObject parent = GetParent(child);
                if (parent == null)
                    return null;

                // check if the parent matches the type and predicate we're looking for
                if ((parent is T) && (predicate((T)parent)))
                    return parent as T;
                else
                    return FindParent<T>(parent);
            }

            static DependencyObject GetParent(DependencyObject child)
            {
                DependencyObject parent = null;
                if (child is Visual || child is Visual3D)
                    parent = VisualTreeHelper.GetParent(child);

                // if fails to find a parent via the visual tree, try to logical tree.
                return parent ?? LogicalTreeHelper.GetParent(child);
            }

        }

        private void menuItemAdd_Click(object sender, RoutedEventArgs e)
        {
            textBoxFirstName.Clear(); textBoxSecondName.Clear(); textBoxApp.Clear(); textBoxBirthDate.Clear(); textBoxCity.Clear(); textBoxEmail.Clear();
            textBoxPhone.Clear(); textBoxPostal.Clear(); textBoxStreet.Clear();imgPhoto.Source = new BitmapImage();
            UpdateAddViewGrid.Visibility = Visibility.Visible;
            ViewListGrid.Visibility = Visibility.Hidden;
            battonAdd.Visibility = Visibility.Visible;
        }

        private void menuItemUpdate_Click(object sender, RoutedEventArgs e)
        {
            UpdateAddViewGrid.Visibility = Visibility.Visible;
            ViewListGrid.Visibility = Visibility.Hidden;
            battonUpdate.Visibility = Visibility.Visible;
        }
        private void menuItemInvite_Click(object sender, RoutedEventArgs e)
        {
            lableInvited.Content = "";
            invitedList.Add(db.getAllPerson()[index].FirstName + " " + db.getAllPerson()[index].SecondName);
            for( int i = 0;i < invitedList.Count; i++)
            {
                lableInvited.Content += invitedList[i]+ System.Environment.NewLine;
            }  
        }

        private void buttonAddNew_Click(object sender, RoutedEventArgs e)
        {
            UpdateAddViewGrid.Visibility = Visibility.Visible;
            ViewListGrid.Visibility = Visibility.Hidden;
            battonAdd.Visibility = Visibility.Visible;
        }

        // Adds the pushpin to the map.
        //myMap.Children.Add(pin);
    }


        //------------------------------------Ievgen Finish-----------------------------------------------------------
    }
  //}
