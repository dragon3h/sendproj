﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectUp
{
    class EventCal
    {
        public EventCal()
        {

        }
        public EventCal(DateTime startTime, DateTime endTime,
            string title, string description, double latitude, double longitude)
        {
            StartTime = startTime;
            EndTime = endTime;
            Title = title;
            Description = description;
            Latitude = latitude;
            Longitude = longitude;
        }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public double Latitude { get; set; }
        public double Longitude  { get; set; }
        public int ID { get; set; }
        public string Invited { get; set; }
    }
}
